<?php
/**
 * Class Mvc_Base
 * PHP base class for all MVC classes
 *
 * @author  Matthias Weiß <info@codeandcreate.de>
 *
 * @version 1.6
 */
abstract class Mvc_Base extends NFY_Mixed_Base
{
	/**
	 * Contains the $moddata array
	 *
	 * @var array
	 */
	protected $moddata = [];

	/**
	 * Contains all valid runtime variable names that can be saved in NFYC_Mixed_Registry
	 *
	 * @var array
	 */
	protected $objectDataFields = [];

	/**
	 * Helper array for save() to check which of the $objectDataFields is required
	 *
	 * @var array
	 */
	protected $objectRequiredDataFields = [];

	/**
	 * Contains valid ajax callable functions
	 *
	 * @var array
	 */
	protected $validBackendCalls = ['save', 'validate'];

	/**
	 * For using a data prefix
	 *
	 * @var string
	 */
	protected $objectDataPrefix = '';


	/**
	 * @var NFYC_Mixed_Registry|null
	 */
	protected $registryInstance = null;

	/**
	 * Temporary path for uploads
	 *
	 * @var string
	 */
	protected $relativeUploadDirBasePath = 'cms_cache/mvcUploadTmp';

	/**
	 * Generic icon for uploaded files
	 *
	 * @var string
	 */
	protected $relativeUploadBaseFileIcon = '/pu_all/images/dropzone/file.png';

	/**
	 * Deaktiviert.
	 * ['.jpg', '.jpeg', '.png', '.gif'];
	 *
	 * @var array
	 */
	protected $validUploadImagePreviewExtensions = [];

	/**
	 * Mvc_Base constructor.
	 *
	 * @param array $moddata
	 * @throws Exception
	 */
	public function __construct($moddata = [])
	{
		$this->registryInstance = NFYC_Mixed_Registry::getInstance();

		$_objectDataPrefix = $this->registryInstance->get("MvcObjectDataPrefix:" . get_class($this));

		if ($_objectDataPrefix) {
			$this->_setObjectDataPrefix($_objectDataPrefix);
		} else {
			$this->_setObjectDataPrefix();
		}

		$this->moddata = $moddata;
	}

	/**
	 * Checks if $function is callable form ajax
	 *
	 * @param $function
	 * @return bool
	 */
	public function isValidBackendFunction($function)
	{
		return in_array($function, $this->validBackendCalls);
	}

	/**
	 * Saves a data prefix if the mvc box is called twice on the same page with different data (wrapper)
	 * For example to make a billing address box and a shipping address box with the same customer data fields
	 *
	 * @param bool $dataPrefix
	 * @throws Exception
	 */
	public function setObjectDataPrefix($dataPrefix = false)
	{
		$this->_setObjectDataPrefix($dataPrefix);
	}

	/**
	 * Saves a data prefix if the mvc box is called twice on the same page with different data
	 * For example to make a billing address box and a shipping address box with the same customer data fields
	 *
	 * @param bool $dataPrefix
	 * @throws Exception
	 */
	private function _setObjectDataPrefix($dataPrefix = false)
	{
		if (!$dataPrefix) {
			$dataPrefix = get_class($this);
		}
		$this->registryInstance->set("MvcObjectDataPrefix:" . get_class($this), $dataPrefix);
		$this->objectDataPrefix = $dataPrefix;
	}

	/**
	 * Returns the set data prefix
	 *
	 * @return string
	 */
	public function getObjectDataPrefix()
	{
		return $this->objectDataPrefix;
	}

	/**
	 * Adds a valid field name to $objectRequiredDataFields on runtime if it is available in $objectDataFields
	 *
	 * @param $name
	 * @throws Exception
	 */
	public function setObjectRequiredDataFields($name)
	{
		if (!is_array($name)) {
			$name = [$name];
		}
		foreach ($name AS $_name) {
			if (in_array($_name, $this->objectDataFields)) {
				$this->objectRequiredDataFields[] = $_name;
			} else {
				throw new Exception(get_class($this) . '::setObjectRequiredDataFields - Fieldname "' . $_name . '" not available.');
			}
		}
	}

	/**
	 * Saves $value on $name in the registry
	 *
	 * @param $name
	 * @param $value
	 * @return bool
	 * @throws Exception
	 */
	protected function setObjectData($name, $value)
	{
		if (in_array($name, $this->objectDataFields)) {
			$this->registryInstance->set($this->objectDataPrefix . ":" . $name, $value);

			return true;
		} else {
			throw new Exception(get_class($this) . '::setObjectData - Fieldname "' . $name . '" not available.');
		}
	}

	/**
	 * Returns the $value of $name
	 * If $name is false / not set it returns all values
	 *
	 * @param bool|string $name
	 * @throws Exception
	 * @return bool|mixed
	 */
	public function getObjectData($name = false)
	{
		$returnValue = null;
		if (!$name) {
			$name = $this->objectDataFields;
		}
		if (is_array($name)) {
			$returnValue = [];
			foreach ($name AS $_name) {
				if (in_array($_name, $this->objectDataFields)) {
					$returnValue[$_name] = $this->registryInstance->get($this->objectDataPrefix . ":" . $_name);
				} else {
					throw new Exception(get_class($this) . '::getObjectData - Fieldname "' . $name . '" not available.');
				}
			}
		} else if (!empty($name)) {
			if (in_array($name, $this->objectDataFields)) {
				$returnValue = $this->registryInstance->get($this->objectDataPrefix . ":" . $name);
			} else {
				throw new Exception(
					get_class($this) . '::getObjectData - Fieldname "' . $name . '" not available. (' . implode(
						', ',
						$this->objectDataFields
					) . ')'
				);
			}
		}

		return $returnValue;
	}

	/**
	 * Checks if all $objectDataFields are valid with validate()
	 *
	 * @return bool
	 * @deprecated
	 * @throws Exception
	 */
	public function isObjectDataValid()
	{
		foreach ($this->objectRequiredDataFields AS $requiredField => $_tmp) {
			$checkValue       = $this->getObjectData($requiredField);
			$validationReturn = $this->validate($requiredField, $checkValue);

			if ($validationReturn != true) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Base functionality to check if a $objectDataFields value is valid ("not empty value")
	 * Should be overwritten in a mvc class
	 *
	 * @param $name
	 * @param $value
	 * @return bool
	 */
	public function validate($name, $value)
	{
		if (!empty($value) AND !empty($name)) {
			return true;
		}

		return false;
	}

	/**
	 * Resets the $objectDataFields
	 *
	 * @throws Exception
	 */
	public function reset()
	{
		foreach ($this->objectDataFields AS $fieldName) {
			$this->setObjectData($fieldName, false);
		}
	}

	/**
	 * $moddata is always read only; just a simple getter
	 *
	 * @return array
	 */
	public function getModdata()
	{
		return $this->moddata;
	}
}