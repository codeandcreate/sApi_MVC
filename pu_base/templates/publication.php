<?php
/**
 * SAMPLE CODE
 * 
 * somewhat like the redFACT /pu_.../publication.php
 *
 * @var $this miniTemplateEngine
 */

// NFY_Frontend_Render replacement
$teInstance = new miniTemplateEngine();

/**
 * just some pseudo tpl vars
 */
$this->tplVars['titleTag'] = "sample page";


$this->tplVars['_emlConfig'] = '
<script>
  const _emlConfig = {
    "mutationObserver": {                        
      "src": "/pu_all/scripts//s_mutationObserver.js",
      "fns": [
        "add", "remove"
      ]
    },
    "replaceWith": {
      "src": "/pu_all/scripts//s_replaceWith.js",
      "fns": [
        "mixedNodes", "iframe", "image", "embed", "script", "elementInsteadOf"
      ]
    },
    "require": {
      "src": "/pu_all/scripts//s_require.js",
      "fns": [
        "js", "css", "object"
      ]
    },
    "mvc": {
      "src": [
        "/pu_all/scripts/s_mvc.min.js"
      ],
      "fns": [
        "addListener",
        "initMvcObject"
      ]
    }
  };
  const mvcPlatformSupportFile = "/pu_base/scripts/s_mvc_platformSupport.js";
</script>';
$this->tplVars['mvcLoad'] = '';
/*
if you want to load the on every page - even when it isn't needed at all - place this in the mvcLoad variable and remove the _emlConfig above:
<script src="/pu_all/scripts/s_mvc.js"></script>
*/

$this->tplVars['bodyContent'] = $teInstance->render("sample", "page");
$this->tplVars['footerContent'] = "";