/**
 * Custom ui functionality like displaying error messages
 */
$s.onApiReady("mvc", () => {
	$s.extend({
		mvc: {
			platformSupport: {
				markAsValid: function (domObject, insertAfterDomObject, additionalClass)
				{
					
				},
				displayErrorMessage: function (domObject, message, insertAfterDomObject, insertBefore)
				{
					
				},
				displayNoticeMessage: function (domObject, message, insertAfterDomObject, insertBefore)
				{
					
				},
				displaySuccessMessage: function (domObject, message, insertAfterDomObject, insertBefore)
				{
					
				},
				resetErrorMessage: function (domObject, insertedAfterDomObject)
				{
					
				},
				popupMessage: function (messageObject, closeButtonCaption, closeButtonAction)
				{
					
				},
				closePopupMessage: function ()
				{
					
				},
				showLoading: function ()
				{
					
				},
				hideLoading: function ()
				{
					
				}
			}
		}
	});
});