# Using the MVC
First of all take a look at the sample:

- /pu_base/templates/page/sample.php
- /pu_base/tpl/page/sample.html
- /pu_base/classes/base_page_sample.php
- /pu_base/scripts/base_page_sample.js

On default, a template on redFACT has just a file in _templates_ and in _tpl_ for the current module. The mvc extends this with two additional files in _classes_ and _scripts_. The first one holds the dynamic logic the mvc backend controller talks to if calls come via ajax. The javascript make the magic on client side;

- Template (PHP): /pu_.../templates/.../templatename.php
- Template (TAL): /pu_.../tpl/.../templatename.tpl (.html in without PHPTAL/redFACT)
- Class (PHP): /pu_.../classes/publication_module_templatename.php
- Class (Javascript): /pu_.../scripts/publication_module_templatename.js

## Naming
The naming of your class and java script is based on the publication, module and template name. So if your publication is "website", the template is a contentbox (pu_website/templates/contentbox/...) and is named "sidebar", you need to have following files with content:

- /pu_website/classes/website_contentbox_sidebar.php
- /pu_website/scripts/website_contentbox_sidebar.js

## Basic TPL-Structure
To make sure the javascript is loaded and that the _initMvcObject_ function (more on that later) find the dom node you need a basic structure for your template:
``` html
<section data-mvcid="${objectClassName}">
	<input data-mvcswitcher="blur" name="sampleInput" value="" type="text" placeholder="Please enter something" />
</section>
<script src="${javascriptObjectName}"></script>
```

### data-mvcid/objectClassName and javascriptObjectName
The attribute _data-mvcid_ and _objectClassName_ is necessary to find the dom node. This is contains the name of your template. The _frontendbox.php_ generates this for you, normally. The same goes with _javascriptObjectName_. If the variable is empty the _NFYC_Mixed_MvcClassLoader_ couldn't find the file in your /pu_*/scripts directory.

### data-mvcswitcher
All nodes with such a data attribute will automatically be bound on the mvc object by calling _initMvcObject_. This makes live easy for simple forms and interactive boxes. If you like to have more control, for example if you want to execute something before the data will be send on blur (...), you need to use _addListener()_ instead.

## Basic php class structure
On default the abstract _Mvc_Base_ handles all for you. So all you need is to extend your class on _Mvc_Base_ instead of _NFY_Mixed_Base_. Infact the _Mvc_Base_ extends _NFY_Mixed_Base_ itself. But to work with the mvc you should use the following as example. For more functionality look at the _Mvc_Base_ and the inline comments.

``` php
class website_contentbox_sidebar extends Mvc_Base
{
	// This is necessary. The registry only works on registered variable names!
	protected $objectDataFields = ['username', 'password', 'email'];

	public function __construct($moddata = [])
	{
		// You need this to make sure to have a working registry to use for example setObjectData()
		parent::__construct($moddata);
	}

	// This function is normally called on blur, clickValidate, ... listeners
	public function validate($name, $value)
	{
		$returnData = ['code' => false, 'message' => ''];

		// some validation magic
		
		if ($returnData['code'] === true) {
			$this->setObjectData($name, $value);
		}

		return $returnData;
	}

	// this function is called on "save" listeners
	public function save($name, $value) 
	{
		return $this->getObjectData();
	}
}
```

## Basic javascript structure
On the client side you need to initialize the dom nodes, bind the listeners and do something with the backend return. So the basic structure is something like this:

``` js
$s.ready(function () // Should be used to make sure anything is loaded, normally
{
	// First parameter is the name of your box; objectClassName in your template
	// Second parameter is the object initMvcObject creates on your dom node - naming doesn't really matter
	$s.mvc.initMvcObject("base_page_sample", (base_page_trail) => 
	{
		// This function is called if something comes from the backend. 
		base_page_trail.postProcess = function (mvcObject, childObject, postObject, returnValue, event)
		{
			//...
		};
		
		// This shows how to add a listener manually:
		// First the mvc object itself, then a selector, the listener type and 
		// a optional function that is called before query before the data is send to the backend.
		$s.mvc.addListener(base_page_trail, "input[name='username']", "blur", function (baseDataStructure)
		{
			// Some javascript code that (maybe) manipulates the baseDataStructure
	
			// Return true if the frontend controller should just send the data like it would without this function.
			// Return a manipulated baseDataStructure to send other data.
			return true;
		});
	});	
});
```
