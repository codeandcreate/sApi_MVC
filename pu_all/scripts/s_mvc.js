/**
 * sApi mvc frontend controller
 * - based on the SMD MVC2
 *
 * @author Matthias Weiß <info@codeandcreate.de>
 * @version 3.0.1
 */
$s.extend({
	mvc: {
		/**
		 * placeholder for the platform support
		 */
		platformSupport: {},
		/**
		 * The standard structure of a MVC object
		 */
		baseInterface: {
			/**
			 * Holds all with addListener registered child elements
			 */
			registeredChilden: [],
			
			/**
			 * Placeholder for the default callback function call of the frontendController
			 *
			 * @param mvcObject
			 * @param childObject
			 * @param postObject
			 * @param returnValue
			 * @param event
			 */
			postProcess: function (mvcObject, childObject, postObject, returnValue, event)
			{
			},
			/**
			 * wrapper for console.log
			 * 
			 * @param errorMessage
			 * @private
			 */
			_logError: function (errorMessage)
			{
				console.log(errorMessage);
			},
			/**
			 * Returns if domObject is a child of the current mvc object
			 *
			 * @param domObject
			 * @returns {boolean}
			 */
			isAChild: function (domObject)
			{
				return this.registeredChilden.indexOf(domObject) !== -1;
			},
			/**
			 * Wrapper für mvc2platformSupport: Markiert ein Element als valide
			 *
			 * @param fieldID
			 * @param afterOtherObject
			 * @param additionalClass
			 */
			markAsValid: function (fieldID, afterOtherObject, additionalClass)
			{
				if (Object.hasOwn(this._parent.platformSupport, "markAsValid")) {
					$this._parent.platformSupport.markAsValid(fieldID, afterOtherObject, additionalClass);
				} else {
					this._logError("MVC WARNING: mvc2platformSupport.markAsValid not available");
				}
			},
			
			/**
			 * Wrapper für mvc2platformSupport: Zeigt eine Fehlermeldung an
			 *
			 * @param fieldID
			 * @param message
			 * @param insertAfterFieldId
			 * @param placement
			 * @param callback
			 */
			displayErrorMessage: function (fieldID, message, insertAfterFieldId, placement, callback)
			{
				if (Object.hasOwn(this._parent.platformSupport, "displayErrorMessage")) {
					this._parent.platformSupport.displayErrorMessage(fieldID, message, insertAfterFieldId, placement, callback);
				} else {
					this._logError("MVC WARNING: mvc2platformSupport.displayErrorMessage not available");
					alert(message);
				}
			},
			
			/**
			 * Wrapper für mvc2platformSupport: Zeigt eine Erfolgsmeldung an
			 *
			 * @param fieldID
			 * @param message
			 * @param insertAfterFieldId
			 * @param placement
			 * @param callback
			 */
			displaySuccessMessage: function (fieldID, message, insertAfterFieldId, placement, callback)
			{
				if (Object.hasOwn(this._parent.platformSupport, "displaySuccessMessage")) {
					this._parent.platformSupport.displaySuccessMessage(fieldID, message, insertAfterFieldId, placement, callback);
				} else {
					this._logError("MVC WARNING: mvc2platformSupport.displaySuccessMessage not available");
					alert(message);
				}
			},
			
			/**
			 * Wrapper für mvc2platformSupport: Zeigt eine Hinweismeldung an
			 *
			 * @param fieldID
			 * @param message
			 * @param insertAfterFieldId
			 * @param placement
			 * @param callback
			 */
			displayNoticeMessage: function (fieldID, message, insertAfterFieldId, placement, callback)
			{
				if (Object.hasOwn(this._parent.platformSupport, "displayNoticeMessage")) {
					this._parent.platformSupport.displayNoticeMessage(fieldID, message, insertAfterFieldId, placement, callback);
				} else {
					this._logError("MVC WARNING: mvc2platformSupport.displayNoticeMessage not available");
					alert(message);
				}
			},
			
			/**
			 * Wrapper für mvc2platformSupport: Löscht alle Hinweise zu einem Element
			 *
			 * @param domObject
			 * @param insertedAfterDomObject
			 */
			resetErrorMessage: function (domObject, insertedAfterDomObject)
			{
				if (Object.hasOwn(this._parent.platformSupport, "resetErrorMessage")) {
					this._parent.platformSupport.resetErrorMessage(domObject, insertedAfterDomObject);
				} else {
					this._logError("MVC WARNING: mvc2platformSupport.resetErrorMessage not available");
				}
			},
			
			/**
			 * Wrapper für mvc2platformSupport: Öffnet eine Popup Benachrichtigung
			 *
			 * @param message
			 * @param closeButtonCaption
			 * @param closeButtonAction
			 */
			popupMessage: function (message, closeButtonCaption, closeButtonAction)
			{
				if (Object.hasOwn(this._parent.popupMessage, "resetErrorMessage")) {
					this._parent.platformSupport.popupMessage(message, closeButtonCaption, closeButtonAction);
				} else {
					this._logError("MVC WARNING: mvc2platformSupport.popupMessage not available");
					alert(message);
				}
			},
			
			/**
			 * Wrapper für mvc2platformSupport: Öffnet eine Lade-Layer
			 */
			showLoading: function ()
			{
				if (Object.hasOwn(this._parent.popupMessage, "showLoading")) {
					this._parent.platformSupport.showLoading();
				} else {
					this._logError("MVC WARNING: mvc2platformSupport.showLoading not available");
				}
			},
			
			/**
			 * Wrapper für mvc2platformSupport: Schließt den Lade-Layer
			 */
			hideLoading: function ()
			{
				if (Object.hasOwn(this._parent.popupMessage, "hideLoading")) {
					this._parent.platformSupport.hideLoading();
				} else {
					this._logError("MVC WARNING: mvc2platformSupport.showLoading not available");
				}
			}
		},
	
	
		/**
		 * List of all registred mvc objects
		 */
		mvcObjects: [],
	
		/**
		 * MVC message Log
		 */
		_messageLog: [],
	
		/**
		 * Adds a listener to a dom element
		 *
		 * @param mvcObject         MVC Objekt
		 * @param subObjectSelector CSS celektor child to add a listener to
		 * @param switcher          blur, change, keyup, keydown, input, click, save (click), clickValidate (click with calling the backend controller), upload
		 * @param preFunction       Function that will be executed before the value if the dom node is read and the backend controller is queried
		 *
		 * @returns {*} | null
		 */
		addListener: function (mvcObject, subObjectSelector, switcher, preFunction)
		{
			const that = this;
			var domObject = subObjectSelector;
			if (typeof domObject !== "object") {
				domObject = this.getChildFromMvcObject(mvcObject, subObjectSelector);
			}
	
			/**
			 * if there is more than on dom node found the listener will be bound on all of them
			 */
			if (domObject !== null && typeof domObject.isList !== "undefined" && domObject.isList === true) {
				var returnObjects = [];
				domObject.forEach(function (listElement)
				{
					returnObjects.push(this.addListener(mvcObject, listElement, switcher, preFunction));
				});
				return returnObjects;
			}
	
			/**
			 * Returns the vale of a dom node
			 * Multiple selects will be returned as #-separeted string
			 *
			 * @param elements array of DOM-Objects
			 * @returns String | boolean
			 */
			function getValueOfElement(elements)
			{
				for (var _i in elements) {
					if (elements[_i].nodeName === "SELECT") {
						var returnValues = "";
						if ((elements[_i].getAttribute('multiple') !== null) || (elements[_i].getAttribute('multiple') !== '')) {
							for (var i = 0; i < elements[_i].length; i++) {
								if (elements[_i].options[i].selected) {
									if (returnValues === "") {
										returnValues = elements[_i].options[i].value;
									} else {
										returnValues = returnValues + "#" + elements[_i].options[i].value;
									}
								}
							}
							return returnValues;
						}
					} else if (elements[_i].nodeName === "INPUT" && elements[_i].getAttribute('type') === "checkbox") {
						if (elements[_i].checked) {
							return elements[_i].value;
						} else {
							return false;
						}
					} else if (elements[_i].hasAttribute('data-value')) {
						if (
							elements[_i].getAttribute('data-checked') === "checked" ||
							elements[_i].getAttribute('data-selected') === "selected" ||
							(!elements[_i].hasAttribute('data-checked') && !elements[_i].hasAttribute('data-selected'))
						) {
							return elements[_i].getAttribute('data-value');
						} else {
							return false;
						}
					} else if (elements[_i].parentNode.hasAttribute('data-value')) {
						var pElement = elements[_i].parentNode;
						if (
							pElement.getAttribute('data-checked') === "checked" ||
							pElement.getAttribute('data-selected') === "selected" ||
							(!pElement.hasAttribute('data-checked') && !pElement.hasAttribute('data-selected'))
						) {
							return pElement.getAttribute('data-value');
						} else {
							return false;
						}
					}
				}
				return elements[0].value;
			}
	
			function getNameOfElement(element)
			{
				if (element.hasAttribute('name')) {
					return element.getAttribute('name');
				} else if (element.hasAttribute('data-name')) {
					return element.getAttribute('data-name');
				}
	
				return null;
			}
	
			if (typeof domObject === "object" && domObject !== null && typeof mvcObject.postProcess !== "undefined") {
	
				/**
				 * Dom nodes must have 3 object arrays for adding a mvc2frontendController listener
				 *
				 * postObjects                  contains the values for the listener which will be send to the backend controller
				 * frontendControllerListener   contains the functions of the listeners
				 * frontendControllerObservers  contains all observer objects for this dom node
				 * */
				if (typeof domObject.postObjects !== "object") {
					domObject.postObjects = {};
				}
				if (typeof domObject.frontendControllerListener !== "object") {
					domObject.frontendControllerListener = {};
				}
				if (typeof domObject.frontendControllerObservers === "undefined") {
					domObject.frontendControllerObservers = [];
				}
	
				//generates an internal id of the dom node
				domObject.mvcObjectRealId = mvcObject.mvcObjectRealId + "-" + function ()
				{
					var str = subObjectSelector;
	
					if (str.length % 32 > 0) {
						str += Array(33 - str.length % 32).join("z");
					}
					var hash = '', bytes = [], j = 0, k = 0, a = 0, ch = 0,
							dict                                           = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
					for (var i = 0; i < str.length; i++) {
						ch         = str.charCodeAt(i);
						bytes[j++] = (ch < 127) ? ch & 0xFF : 127;
					}
					var chunk_len = Math.ceil(bytes.length / 32);
					for (i = 0; i < bytes.length; i++) {
						j += bytes[i];
						k++;
						if ((k === chunk_len) || (i === bytes.length - 1)) {
							a = Math.floor(j / k);
							if (a < 32) {
								hash += '0';
							} else if (a > 126) {
								hash += 'z';
							} else {
								hash += dict[Math.floor((a - 32) / 2.76)];
							}
							j = k = 0;
						}
					}
					return hash;
				}();
	
				/**
				 * Default postObjects object with all relevant contents for a post to the backend
				 *
				 * @type {{objectId: *, switcher: *, name, value: string, preFunctionReturn: boolean}}
				 */
				domObject.postObjects[switcher] = {
					'objectId'         : mvcObject.mvcObjectRealId,
					'switcher'         : switcher,
					'name'             : getNameOfElement(domObject),
					'value'            : '',
					'preFunctionReturn': true
				};
	
				if (
					switcher === 'blur' ||
					switcher === 'change' ||
					switcher === 'input' ||
					switcher === 'click' ||
					switcher === 'clickValidate' ||
					switcher === 'keyup' ||
					switcher === 'keydown' ||
					switcher === 'save'
				) {
					/**
					 * Builds the function for the listener
					 *
					 * @param event
					 */
					domObject.frontendControllerListener[switcher] = function (event)
					{
						/**
						 * The real listener for the event, which is called after the value is read
						 * and the eventually set preFunction is called.
						 *
						 * @param _defaultFunctionParams
						 */
						const defaultFunction = function (_defaultFunctionParams)
						{
							if (_defaultFunctionParams.postObjects.switcher === "click") {
								that.callBackToObject(mvcObject, _defaultFunctionParams.mvcObject, _defaultFunctionParams.childObject, _defaultFunctionParams.postObjects, null);
							} else {
								var _backendFunction = 'validate';
								if (_defaultFunctionParams.postObjects.switcher === "save") {
									_backendFunction = 'save';
								}
								that.queryBackEndController(_backendFunction, _defaultFunctionParams.postObjects, 'POST', function (returnValue)
								{
									that.callBackToObject(mvcObject, _defaultFunctionParams.mvcObject, _defaultFunctionParams.childObject, _defaultFunctionParams.postObjects, returnValue);
								});
							}
						};
	
						/**
						 * Calling if the preFunction if set.
						 * If the return is false, the defaultFunction is never called automatically
						 */
						if (typeof preFunction === "function") {
							domObject.postObjects[switcher]['preFunctionReturn'] = preFunction({
								'mvcObject'      : mvcObject,
								'childObject'    : domObject,
								'switcher'       : switcher,
								'event'          : event,
								'postObjects'    : domObject.postObjects[switcher],
								'defaultFunction': defaultFunction
							});
						}
	
						/**
						 * Reads the value of the dom node
						 */
						if (typeof event.srcElement !== "undefined") {
							domObject.postObjects[switcher].value = getValueOfElement([event.srcElement, domObject]);
						} else if (typeof event.target !== "undefined") {
							domObject.postObjects[switcher].value = getValueOfElement([event.target, domObject]);
						}
	
						/**
						 * Executes the defaultFunction
						 */
						if (domObject.postObjects[switcher]['preFunctionReturn'] !== false) {
							delete(domObject.postObjects[switcher]['preFunctionReturn']);
							defaultFunction({
								'mvcObject'  : mvcObject,
								'childObject': domObject,
								'switcher'   : switcher,
								'event'      : event,
								'postObjects': domObject.postObjects[switcher]
							});
						}
					};
	
					/**
					 * Bindes the listener to the dom node
					 */
					if (domObject.addEventListener) {
						var _switcher = switcher;
						if (_switcher === "save" || _switcher === "clickValidate") {
							_switcher = "click"
						}
	
						domObject.addEventListener(_switcher, domObject.frontendControllerListener[switcher], false);
						mvcObject.registeredChilden.push(domObject);
	
						return domObject;
					}
				} else if (switcher === 'upload') {
					domObject.style.display                        = 'none';
					domObject.frontendControllerListener['change'] = function (event)
					{
						var defaultFunction = function (_defaultFunctionParams)
						{
							// Reset the error message CSS in case one is displayed.
							var divDropzoneError = $s.qs(".mvcDropzone[data-for='" + domObject.mvcObjectRealId + "']", mvcObject);
							divDropzoneError.classList.remove('error');
	
							// Send each file to the backend one at a time.
							for (var i = 0; i < _defaultFunctionParams.childObject.files.length; i++) {
								that._uploadSupport.sendUploadFileViaXHR(mvcObject, _defaultFunctionParams.childObject, _defaultFunctionParams.childObject.files[i], _defaultFunctionParams.switcher);
							}
						};
	
						// Fire a pre-function if one exists.
						if (typeof preFunction === "function") {
							domObject.postObjects[switcher]['preFunctionReturn'] = preFunction({
								'mvcObject'      : mvcObject,
								'childObject'    : domObject,
								'switcher'       : switcher,
								'event'          : event,
								'postObjects'    : domObject.postObjects[switcher],
								'defaultFunction': defaultFunction
							});
						}
	
						/**
						 * Executres the defaultFunction
						 */
						if (domObject.postObjects[switcher]['preFunctionReturn'] !== false) {
							defaultFunction({
								'mvcObject'  : mvcObject,
								'childObject': domObject,
								'switcher'   : switcher,
								'event'      : event,
								'postObjects': domObject.postObjects[switcher]
							});
						}
					};
					domObject.addEventListener('change', domObject.frontendControllerListener['change'], false);
					that._uploadSupport.init(mvcObject, domObject, switcher);
	
					return domObject;
				}
			}
	
			return null;
		},
	
		/**
		 * Unbindes a listener
		 *
		 * @param childObject
		 * @param switcher
		 */
		removeListener: function (childObject, switcher)
		{
			if (typeof childObject.frontendControllerListener === "object") {
				var listenerType = switcher;
				if (listenerType === "save") {
					listenerType = "click"
				}
				if (typeof childObject.frontendControllerListener[switcher] === "function") {
					childObject.removeEventListener(listenerType, childObject.frontendControllerListener[switcher]);
				}
			}
		},
	
		/**
		 * Adds an observer to the observerObject for the observedObject
		 *
		 * @param observedObject
		 * @param observerObject
		 */
		addObserver: function (observedObject, observerObject)
		{
			if (typeof observedObject === "string") {
				observedObject = this.getMvcObjectFromId(observedObject);
			} else if (
				typeof observedObject.mvcClass === "string" &&
				typeof observedObject.mvcChildSelector === "string"
			) {
				var _mvcObject = this.getMvcObjectFromId(observedObject.mvcClass);
				if (_mvcObject !== null) {
					observedObject = this._parent.qs(observedObject.mvcChildSelector, _mvcObject);
				} else {
					observedObject = null;
				}
			}
			if (typeof observerObject === "string") {
				observerObject = this.getMvcObjectFromId(observerObject);
			}
	
			if (observedObject !== null && observerObject !== null) {
				observedObject.forEach(function (_observedObject)
				{
					if (
						typeof _observedObject === "object" &&
						typeof observerObject === "object" &&
						typeof _observedObject.frontendControllerObservers === "object"
					) {
						if (typeof _observedObject.frontendControllerObservers[observerObject] !== "undefined") {
							this.removeObserver(observedObject, observerObject);
						}
						_observedObject.frontendControllerObservers.push(observerObject);
					}
				});
			}
		},
	
		/**
		 * Deletes an observer on the observedObject
		 *
		 * @param observedObject
		 * @param observerObject
		 */
		removeObserver: function (observedObject, observerObject)
		{
			if (
				typeof observedObject === "object" &&
				typeof observerObject === "object" &&
				typeof observerObject.mvcObjectRealId === "string"
			) {
				var _indexOfObject = null;
				for (var i in observedObject.frontendControllerObservers) {
					if (typeof observedObject.frontendControllerObservers[i].mvcObjectRealId === "string") {
						if (observerObject.mvcObjectRealId === observedObject.frontendControllerObservers[i].mvcObjectRealId) {
							_indexOfObject = i;
							break;
						}
					}
				}
	
				if (_indexOfObject !== null) {
					observedObject.frontendControllerObservers.splice(
						_indexOfObject,
						1
					);
					return true;
				}
			}
			return false;
		},
	
		/**
		 * Executes an event of a with addListener added listener on a domObjects manually
		 *
		 * @param domObject
		 * @param switcher
		 */
		callEvent: function (domObject, switcher)
		{
			if (switcher === "clickValidate" || switcher === "save") {
				switcher = "click";
			}
			if (typeof domObject === "object" && document.createEvent) {
				var evt = document.createEvent("Events");
				evt.initEvent(switcher, true, true);
				domObject.dispatchEvent(evt);
			}
		},
	
		/**
		 * Extends an javascript object with mvcBaseInterface similar to jQuery Extend
		 *
		 * @param object
		 * @param returnObject
		 * @returns {*} | null
		 */
		initMvcObject: function (object, callback)
		{
			if (typeof object === "string") {
				object = document.querySelector("[data-mvcid='" + object + "']");
			}
			if (typeof object === "object" && object !== null) {
				object.mvcObjectRealId = object.getAttribute("data-mvcid");
				object.removeAttribute("data-mvcid");
				if (typeof this.baseInterface !== "undefined") {
					for (var key in this.baseInterface) {
						object[key] = this.baseInterface[key];
					}
				}
				this.garbageCollector();
				this.mvcObjects.push(object);
	
				var _automaticRegisteredFields = object.querySelectorAll("[data-mvcswitcher]");
				if (_automaticRegisteredFields !== null) {
					const that = this;
					_automaticRegisteredFields.forEach(function(objectToRegister) {
						that.addListener(object, objectToRegister, objectToRegister.getAttribute("data-mvcswitcher"));
						objectToRegister.removeAttribute("data-mvcswitcher");
					});
				}
	
				this.ready(null, object);
	
				if (typeof callback === "function") {
					callback(object);
				} else {
					return object;
				}
			}
			return null;
		},
	
		/**
		 * Executes a ready event if a mvc object has one
		 *
		 * @param callback
		 * @param objectToRunReady
		 */
		ready: function (callback, objectToRunReady)
		{
			if (typeof callback === "function") {
				callback();
			}
			if (typeof objectToRunReady === "object" && typeof objectToRunReady.mvcObjectRealId !== "undefined") {
				var event = new CustomEvent(objectToRunReady.mvcObjectRealId + "::ready");
				document.body.dispatchEvent(event);
				window[objectToRunReady.mvcObjectRealId + "::ready-dispatched"] = true;
			}
		},
	
		/**
		 * Deletes mvc objects from this.mvcObjects if they aren't in the dom anymore
		 *
		 * @private
		 */
		garbageCollector: function ()
		{
			for (var i in this.mvcObjects) {
				var objectToCheck = this.mvcObjects[i];
				/*
				 * Objekte, die nicht im DOM sind, haben irgendwo in der Hierarchieebene (parentNode.parentNode,...)
				 * ein null. Falls das null erreicht wird ohne dass das aktuelle Element <html> ist, dann ist das
				 * Objekt nicht mehr im DOM.
				 */
				while (objectToCheck.tagName !== "HTML") {
					if (objectToCheck.parentNode === null) {
						this.mvcObjects.splice(i, 1);
						break;
					}
					objectToCheck = objectToCheck.parentNode;
				}
			}
		},
	
		/**
		 * Queries the backend controller
		 *
		 * @param query
		 * @param data
		 * @param method
		 * @param callback
		 */
		queryBackEndController: function(query, data, method, callback)
		{
			this._parent.ajax({
					url     : "/pu_all/ajax/mvcbackend.php?" + query,
					callback: callback,
					data    : data,
					method  : method
				});
		},
	
		/**
		 * Calls the postProcess function of a _mvcObject and an registered observer mvc object
		 *
		 * @param _givenMvcObject
		 * @param childObject
		 * @param postObject
		 * @param returnValue
		 */
		callBackToObject: function(_mvcObject, _givenMvcObject, childObject, postObject, returnValue)
		{
			_mvcObject.postProcess(_givenMvcObject, childObject, postObject, returnValue);
	
			if (
				typeof childObject.frontendControllerObservers === "object" &&
				childObject.frontendControllerObservers.length > 0
			) {
				for (var key in childObject.frontendControllerObservers) {
					if (typeof childObject.frontendControllerObservers[key].postProcess === "function") {
						childObject.frontendControllerObservers[key].postProcess(_givenMvcObject, childObject, postObject, returnValue);
					}
				}
			}
		},
	
		/**
		 * Returns a (or the first) registered mvc object with mvcid
		 *
		 * @param mvcid
		 * @returns {*}
		 */
		getMvcObjectFromId: function(mvcid)
		{
			for (var i in this.mvcObjects) {
				if (this.mvcObjects[i].mvcObjectRealId === mvcid) {
					return this.mvcObjects[i];
				}
			}
	
			return null;
		},
	
		/**
		 * Returns a child of a mvcObject on a cssSelector
		 *
		 * @param mvcObject
		 * @param cssSelector
		 * @returns {*}
		 */
		getChildFromMvcObject: function(mvcObject, cssSelector)
		{
			if (typeof mvcObject === "string") {
				mvcObject = this.getMvcObjectFromId(mvcObject);
			}
	
			if (mvcObject !== null) {
				return $this._parent.qs(cssSelector, mvcObject);
			}
	
			return null;
		},
	
		/**
		 * Internal logging functionality
		 *
		 * @param message
		 * @private
		 */
		_logMessage: function(message)
		{
			var d = new Date();
			this._messageLog.push(d.getHours()  + ":" + d.getMinutes() + ":" + d.getSeconds() + " => " + message);
		},
		
		init: function()
		{
			if (typeof mvcPlatformSupportFile === "string") {
				this._parent.require.js(mvcPlatformSupportFile);
			}	
		}
	}
});