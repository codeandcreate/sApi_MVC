# sApi MVC Module (for redFACT)

The [sApi](https://codeberg.org/codeandcreate/sApi) MVC Module is an implementation for a frontend MVC on vanilla javascript. It's based on the original [SMD MVC2](https://github.com/schwaebischmediadigital/smdMVC).

![flow chart of the mvc](documentation/flowchart.png)

It's build for [Newsfactory redFACT](https://www.newsfactory.de/solutions/cms-redfact.html) but can also used stand alone or with any other CMS. In this repository there are some sample scripts to use the MVC without redFACT. Some files may contain german comments. [Pure.css](https://purecss.io/) is used for the sample frontend.

## Version history

|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;Version&nbsp;&nbsp;&nbsp;| Info |
|---|---|---|
| August 2022 | 3.0.1 | Upgraded to sApi 202208.01 |
| Juli 2022 | 3.0 | Refactored the frontend controller as sApi module, dropped upload features (for now), removed some SMD specific code |

## Todos
- (More) documentation

## Installation

### redFACT
* Copy following files in your redFACT installation:
  - pu_all/ajax/mvcbackend.php
  - pu_all/scipts/s_mvc.min.js (or s_mvc.js if you want uncompressed code)
  - pu_all/scipts/sApi.min.js (needs to be loaded in your publication.tpl)
  - pu_base/scripts/s_mvc_platformSupport.js goes to your publication folder
  - redFACT/customized/classes/mixed/mvcClasses/mvc_base.php
  - redFACT/customized/classes/mixed/nfyc_mixed_mvcbackendcontroller.php
  - redFACT/customized/classes/mixed/nfyc_mixed_mvcclassloader.php
  - redFACT/customized/classes/mixed/nfyc_mixed_registry.php
* pu_all/ajax/mvcbackend.php has a XSS protection: you need to add a $domains-Array to your inc/config.php (...) file with the domains that are allowed to access the mvc backend if not already set by your configuration
* Take a look at the loader for contentbox, page or all other frontend related modules in pu_all/templates/frontendbox.php. This loader should always be loaded!
* Configure the _emlConfig and the platform support file before sApi loads like this: 
``` js
const _emlConfig = {
  "mvc": {
    "src": [
      "/pu_all/scripts/s_mvc.min.js"
    ],
    "fns": [
      "addListener",
      "initMvcObject"
    ]
  }
};
const mvcPlatformSupportFile = "/pu_base/scripts/s_mvc_platformSupport.js";
```
  
### without redFACT
* just copy the whole folder to your web server and take a look at the pu_base and inc folders.

### Static installation
Whit the steps above the mvc loads on demand. This is usefull if you don't have on every page mvc boxes. But if you prefer to have a fully dynamic webpage with mvc boxes on every page you shouldn't use the eml-Loader of sApi. Just place the s_mvc.min.js/s_mvc.js in your /pu_all/scripts_min and your s_mvc_platformSupport.js in /pu_*/scripts_min directory.

## Documentation

- [Basic Informations and getting started](documentation/using_the_mvc.md)
- [Javascript: API documentation of $s.mvc](documentation/s_mvc.md)
- [PHP: API Documentation for a MVC class](documentation/mvc_base.md)